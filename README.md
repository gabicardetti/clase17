
# Clase 17

## Antes de correr el proyecto
Tenemos que ir al archivo dbConfig.js y asegurarnos de tener creado el mismo usuario y misma base de datos ya configurados para mysql, de no tener ese mismo usuario o base de datos editar y dejar los nombres adecuados.

Asi esta configurado ahora

    {
        database: 'test',
        user: 'test',
        password: 'test'
    }

## Correr proyecto
```
yarn install
node index.js
```

<br>
La ruta para ver todo seria 

http://localhost:8080/

Dejo el postman para probar todas las rutas

Y arme una ruta extra para consultar el historial del chat 

GET localhost:8080/api/chat



