export const mysqlOptions = {
    client: 'mysql',
    connection: {
        database: 'test',
        user: 'test',
        password: 'test'
    },
};


export const SQLite3Options = {
    client: 'sqlite3',
    connection: {
        filename: "./DB/Chat"
    },
    useNullAsDefault: true
};
