import { generateRandomNumber, generateRandomNumberFloat } from "../util/utils.js";
import ProductRepository from "./productRepository.js"

const repository = new ProductRepository();

export const getAllProducts = async () => repository.getAll();

export const generateNewProduct = async (title, price, thumbnail) => {
  // hago lenght + 1 para que los id empiezen desde 1 y no desde 0
  const product = generateProduct();
  if (title) product.title = title;
  if (price) product.price = price;
  if (thumbnail) product.thumbnail = thumbnail;

  const savedProduct = await repository.save(product);
  return savedProduct;
};

export const getProductById = async (id) => {
  const product = await repository.getById(id);
  return product;
};

export const deleteProductById = async (id) => {
  const product = await repository.deleteById(id);
  return product;
};

export const updateProductById = async (id, title, price, thumbnail) => {
  const product = await repository.getById(id);
  if (!product) return;

  if (title) product.title = title;
  if (price) product.price = price;
  if (thumbnail) product.thumbnail = thumbnail;

  const updatedProduct = await repository.updateById(id, product);

  return updatedProduct;
};

function generateProduct() {
  return {
    title: "Producto " + generateRandomNumber(1, 10),
    price: generateRandomNumberFloat(0.0, 9999.99),
    thumbnail: "Foto " + generateRandomNumber(1, 10),
  };
}
