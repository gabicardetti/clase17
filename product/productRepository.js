import CRUD from "../util/crud.js"
import { mysqlOptions } from "../dbConfig.js";
import knex from "knex";

const createTableIfNotExist = async (connection) => {
    try {
        await connection.schema.createTable("product", table => {
            table.increments("id")
            table.string("title")
            table.integer("price")
            table.string("thumbnail")
        })
        console.log("chat product created");
    } catch (e) {
        if (e.errno)
            console.log("product already exists")
        else
            console.error("product table not created", e)
    }

}

export default class ProductRepository {

    constructor() {
        const client = knex(mysqlOptions);
        createTableIfNotExist(client);
        this.crud = new CRUD(client, "product");
    }

    async getAll() {
        return this.crud.getAll();
    }

    async save(objects) {
        return this.crud.save(objects);
    }

    async getById(id) {
        return this.crud.getById(id);
    }

    async deleteById(id) {
        return this.crud.deleteById(id);

    }

    async updateById(id, object) {
        return this.crud.updateById(id, object);
    }

}