const baseChat = "/chat";
import express from "express";


import * as ChatController from "./chatController.js"

const routes = express.Router();

routes.get(baseChat + "/", ChatController.getAllMessages);

export default routes;