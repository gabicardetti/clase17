import CRUD from "../util/crud.js"
import { SQLite3Options } from "../dbConfig.js";
import knex from "knex";

const createTableIfNotExist = async (connection) => {
    try {
        await connection.schema.createTable("message", table => {
            table.increments("id")
            table.string("email")
            table.integer("message")
            table.string("date")
        })
        console.log("chat table created");
    } catch (e) {
        if (e.errno)
            console.log("table already exists")
        else
            console.error("chat table not created", e)
    }

}
export default class ChatRepository {

    constructor() {
        const client = knex(SQLite3Options);
        createTableIfNotExist(client);
        this.crud = new CRUD(client, "message");
    }

    async getAll() {
        return this.crud.getAll();
    }

    async save(object) {
        return this.crud.save(object);
    }

}