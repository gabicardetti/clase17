import { File } from "../util/fileUtil.js"
import ChatRepository from "./chatRepository.js"

const repository = new ChatRepository();

export const saveNewMessage = (msg) => {
    repository.save(msg);
}

export const getAllMessagesFromFile = async () => {
    const chatHistory = await repository.getAll();
    return chatHistory;
}