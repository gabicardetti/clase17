import express from "express";
import ProductRoutes from "./product/productRoutes.js";
import ChatRoutes from "./chat/chatRoutes.js";

import { getAllProducts } from "./product/productService.js";
import { saveNewMessage } from "./chat/chatService.js"

import { Server } from "socket.io";
import http from "http";

const app = express();
const port = 8080;
const baseUrl = "/api";

const server = http.createServer(app);
const io = new Server(server);
app.locals.io = io;

io.on("connection", async (socket) => {
  console.log("a user connected");

  const products = await getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);


server.listen(port, async () => {
  console.log(`app listening at http://localhost:${port}`);
});
